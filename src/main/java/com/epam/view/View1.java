package com.epam.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View1 {


    public static Logger logger = LogManager.getLogger(View1.class);
    private static Scanner input = new Scanner(System.in);
    private Map<Enum1, String> menu;

    public static void main(String[] args) {
        new View1().show();
    }
    public View1() {
        menu = new LinkedHashMap<>();
        menu.put(Enum1.ONE, " 1 - print house list");
        menu.put(Enum1.TWO, " 2 - sort house list by price");
        menu.put(Enum1.THREE, " 3 - sort house list by distance to kindergarten");
        menu.put(Enum1.FOUR, " 4 - sort house list by distance to school");
        menu.put(Enum1.FIVE, " 5 - sort house list by distance to playground");
        menu.put(Enum1.QUIT, " Q - Exit");

    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                System.out.println("You press button: " + keyMenu);
            } catch (Exception e) {
                logger.error("Error in view");
            }
        } while (!keyMenu.equals("Q"));
    }
}
