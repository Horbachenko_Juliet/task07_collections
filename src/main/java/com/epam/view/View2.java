package com.epam.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View2 {


    public static Logger logger = LogManager.getLogger(View1.class);
    private static Scanner input = new Scanner(System.in);
    private Map<Enum2, String> menu;


    public View2() {
        menu = new LinkedHashMap<>();
        menu.put(Enum2.ONE, " 1 - Open door number one...");
        menu.put(Enum2.TWO, " 2 - Open door number two...");
        menu.put(Enum2.THREE, " 3 - Open door number three...");
        menu.put(Enum2.FOUR, " 4 - Open door number four...");
        menu.put(Enum2.FIVE, " 5 - Open door number five...");
        menu.put(Enum2.SIX, " 6 - Open door number six...");
        menu.put(Enum2.SEVEN, " 7 - Open door number seven...");
        menu.put(Enum2.EIGHT, " 8 - Open door number eight...");
        menu.put(Enum2.NINE, " 9 - Open door number nine...");
        menu.put(Enum2.TEN, " 10 - Open door number ten...");
        menu.put(Enum2.QUIT, " q - Quit");

    }

    public static void main(String[] args) {
        new View2().show();
    }

    private static boolean isDigit(String s) throws NumberFormatException {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private void outputHead() {
        System.out.println("\nThis is a game and you are a main character.");
        System.out.println("There are ten door in round room.");
        System.out.println("Behind some of them is monster which can hurt you.");
        System.out.println("Behind another doors is magical artifact that give you a strength.");
    }

    private void outputMenu() {
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        outputHead();
        String keyMenu;
        do {
            outputMenu();
            System.out.println("It's time to choose...");
            keyMenu = input.nextLine().toUpperCase();
            try {
                System.out.println("You press button: " + keyMenu);
            } catch (Exception e) {
                logger.error("Error in view");
            }
        } while (!keyMenu.equals("Q"));
    }
}
